FROM alpine:latest

EXPOSE 9999
RUN apk add --no-cache vim
USER 65534:65534
HEALTHCHECK CMD /bin/true

CMD /bin/sh
